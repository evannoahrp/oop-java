package OOP;

public class Main {
    public static void main(String[] args) {
        Anggota obj = new Anggota();
        obj.m_anggota("Yanto", 28);
        System.out.println(obj.m_judul());
        String temp = obj.m_anggota(26, "Wawan");
        System.out.println(temp);
    }
}

class ClassBE {
    String nama;
    int umur;

    public String getNama() {
        return nama;
    }

    public int getUmur() {
        return umur;
    }

    String m_judul() {
        return "Class BE Method final Anggota";
    }

    String m_anggota(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        System.out.println("method m_anggota pada class ClassBE");
        return getNama() + ", " + getUmur();
    }
}

class Anggota extends ClassBE {
    @Override
    String m_anggota(String nama, int umur) {
        super.m_anggota(nama, umur);
        System.out.println(this.nama + ", " + this.umur);
        return nama + ", " + umur;
    }

    @Override
    String m_judul() {
        System.out.println(super.m_judul());
        return "ini method dari class Anggota";
    }

    String m_anggota(int umur, String nama) {
        return nama + ", " + umur;
    }
}